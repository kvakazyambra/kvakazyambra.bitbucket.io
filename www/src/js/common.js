// import Vue from '../../node_modules/vue/dist/vue';
import eventBus from './bus.js';
import './view-switcher.js';
import './header.js';
import './filter.js';
import './news-list.js';


document.addEventListener('click', () => {
  eventBus.closeAll();
});

document.addEventListener('keydown', (evt) => {
  if (evt.keyCode === 27) {
    eventBus.closeAll();
  }
});
