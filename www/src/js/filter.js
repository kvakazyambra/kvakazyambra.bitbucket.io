import Vue from '../../node_modules/vue/dist/vue';
import {store} from './store.js';
import ContextPopup from './context-popup.vue';


new Vue({
  el: '.main-filter',

  store,

  components: {
    ContextPopup
  }
});
