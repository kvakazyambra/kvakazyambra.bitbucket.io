import Vue from 'vue';

export default new Vue({
  methods: {
    closeAll() {
      this.$emit('close-all');
    }
  }
});
