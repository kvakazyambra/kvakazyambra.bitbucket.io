const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const jsPath = './src/js/';
const minPath = '/js/';

module.exports = {
  entry: {
    'common': [jsPath + 'common.js']
  },
/*
  externals: {
    vue: 'Vue'
  },
*/
  output: {
    path: path.resolve(__dirname, minPath),
    publicPath: minPath,
    filename: '[name].build.js'
  },
  // watch: process.env.NODE_ENV !== 'production',
  watch: true,
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          postcss: [require('cssnano')({
            'zindex': false,
            'reduceIdents': false,
            'mergeIdents': false,
            'discardDuplicates': false,
            'discardUnused': false,
            'autoprefixer': {
              'browsers': '> 1%, ie 7-11, last 10 versions, ff >= 5, Chrome >= 8, Opera >= 11',
              'add': true,
              'remove': false,
              'cascade': false
            }
          })],
          loaders: {
            'scss': 'vue-style-loader!css-loader!sass-loader'
          },
          extractCSS: true,
          preserveWhitespace: false,
          // other vue-loader options go here
        }
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },

/*
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]?[hash]'
        }
      }
*/
    ],
    loaders: []
  },
  plugins: [
    new webpack.optimize.CommonsChunkPlugin({
      names: ['common'],
      filename: '[name].build.js'
    }),
    new ExtractTextPlugin('style.css')
  ],
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    }
  },
/*
  devServer: {
    historyApiFallback: true,
    noInfo: true
  },
*/
  performance: {
    hints: false
  },
  // devtool: '#eval-source-map'
};

if (process.env.NODE_ENV === 'production') {
  // module.exports.devtool = '#source-map';
  // http://vue-loader.vuejs.org/en/workflow/production.html
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      // sourceMap: true,
      compress: {
        warnings: false
      },
      mangle: {
        toplevel: true
      }
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true
    })
  ]);
}
