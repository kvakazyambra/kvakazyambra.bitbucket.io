import Vue from '../../node_modules/vue/dist/vue';
import Vuex from 'vuex'
import eventBus from './bus.js';

Vue.use(Vuex);


export const store = new Vuex.Store({
  state: {
    viewMode: 'list-mode',
    isTopicsPopupOpened: false,
    isTypesPopupOpened: false,
    isTimesPopupOpened: false,
    isGeographyPopupOpened: false,
    isLanguagesPopupOpened: false,
    currentLanguage: {
      label: 'English',
      abbr: 'EN'
    },
    currentTopic: {label: 'All'},
    currentType: {label: 'All'},
    currentTime: {label: 'All of time'},
    currentGeography: {label: 'Country'}
  },

  mutations: {
    closeAll() {
      eventBus.$emit('close-all');
    },

    changeViewMode(state, viewModeValue) {
      state.viewMode = viewModeValue;
    },

    toggleTopicsPopup(state, isOpened) {
      state.isTopicsPopupOpened = isOpened;
    },

    setCurrentTopic(state, topic) {
      state.currentTopic = topic;
    },

    toggleTypesPopup(state, isOpened) {
      state.isTypesPopupOpened = isOpened;
    },

    setCurrentType(state, type) {
      state.currentType = type;
    },

    toggleTimesPopup(state, isOpened) {
      state.isTimesPopupOpened = isOpened;
    },

    setCurrentTime(state, type) {
      state.currentTime = type;
    },

    toggleLanguagesPopup(state, isOpened) {
      state.isLanguagesPopupOpened = isOpened;
    },

    setCurrentLanguage(state, lang) {
      state.currentLanguage = lang;
    },

    toggleGeographyPopup(state, isOpened) {
      state.isGeographyPopupOpened = isOpened;
    },

    setCurrentGeography(state, geography) {
      state.currentGeography = geography;
    }
  },

  getters: {
    getViewMode(state) {
      return state.viewMode;
    },

    getCurrentTopic(state) {
      return state.currentTopic;
    },

    isTopicsPopupOpened(state) {
      return state.isTopicsPopupOpened;
    },

    getCurrentType(state) {
      return state.currentType;
    },

    isTypesPopupOpened(state) {
      return state.isTypesPopupOpened;
    },

    getCurrentTime(state) {
      return state.currentTime;
    },

    isTimesPopupOpened(state) {
      return state.isTimesPopupOpened;
    },

    getCurrentLanguage(state) {
      return state.currentLanguage;
    },

    isLanguagesPopupOpened(state) {
      return state.isLanguagesPopupOpened;
    },

    getCurrentGeography(state) {
      return state.currentGeography;
    },

    isGeographyPopupOpened(state) {
      return state.isGeographyPopupOpened;
    }
  }
});
