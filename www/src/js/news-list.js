import Vue from '../../node_modules/vue/dist/vue';
import {store} from './store.js';
import ContextPopup from './context-popup.vue';
import JoinControl from './join-control.vue';
import ShareControl from './share-control.vue';


new Vue({
  el: '.news-list',

  store,

  components: {
    ContextPopup,
    JoinControl,
    ShareControl
  },

  computed: {
    viewMode: function() {
      return this.$store.getters.getViewMode;
    }
  }
})
