import Vue from '../../node_modules/vue/dist/vue';
import {store} from './store.js';


new Vue({
  el: '.view-switcher',

  store,

  data: () => ({
    viewMode: 'list-mode'
  }),

  methods: {
    changeViewMode() {
      this.$store.commit('changeViewMode', this.viewMode)
    }
  }
});
