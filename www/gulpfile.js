const fs = require('fs');
const path = require('path');
const del = require('del');
const urlModule = require('url');
const lazypipe = require('lazypipe');
const gulp = require('gulp');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const watch = require('gulp-watch');
const rename = require('gulp-rename');
const browserSync = require('browser-sync').create();
const plumber = require('gulp-plumber');
const progeny = require('gulp-progeny');
const runSequence = require('run-sequence');
const cached = require('gulp-cached');
const count = require('gulp-count');
const postcss = require('gulp-postcss');
const cssnano = require('cssnano');
const iconfont = require('gulp-iconfont');
const consolidate = require('gulp-consolidate');
const webpack = require('webpack-stream');

const workDir = './';

const conf = {
  html: workDir + 'html/**/*.html',
  css: {
    src: workDir + 'src/sass/**/*.scss',
    min: workDir + 'css'
  },
  js: {
    // src: workDir + 'src/js/**/*.js',
    min: workDir + 'js'
  }
};

//process.env.NODE_PATH = src + ':' + (process.env.NODE_PATH || '');



const compileSass = lazypipe()
    .pipe(plumber)
    .pipe(sourcemaps.init)
    .pipe(function() {
      return sass().on('error', sass.logError);
    })
    .pipe(postcss, [cssnano({
      'zindex': false,
      'reduceIdents': false,
      'mergeIdents': false,
      'discardDuplicates': false,
      'discardUnused': false,
      'autoprefixer': {
        'browsers': '> 1%, ie 10-11, last 10 versions, ff >= 5, Chrome >= 8,' +
          ' Opera >= 11',
        'add': true,
        'remove': false,
        'cascade': false
      }
    })])
    .pipe(rename, {dirname: ''})
    .pipe(sourcemaps.write, './', {
      includeContent: false,
      sourceRoot: '/' + workDir,
      debug: true
    });


gulp.task('clean-js', function(callback) {
  del([conf.js.min + '/*.js', conf.js.min + '/*.js.map']);
  callback();
});


gulp.task('clean-css', function(callback) {
  del([conf.css.min + '/*.css', conf.css.min + '/*.css.map']);
  callback();
});


gulp.task('icon-font', function() {
  return gulp.src([workDir + 'img/sprite-vector/*.svg'])
      .pipe(iconfont({
        fontName: 'icons-font', // required
        prependUnicode: true, // recommended option
        formats: ['woff', 'woff2', 'eot'],
        fontHeight: 1000,
        normalize: true,
        // recommended to get consistent builds when watching files
        timestamp: Math.round(Date.now() / 1000),
      }))
      .on('glyphs', function(glyphs, options) {
        options.fontPath = '/fonts/';
        options.className = 'with-icon';
        options.glyphs = glyphs.map(function(glyph) {
          return {
            name: glyph.name,
            codepoint: glyph.unicode[0].charCodeAt(0)
          };
        });

        gulp.src(workDir + 'img/sprite-vector/icons-font.scss')
            .pipe(consolidate('lodash', options))
            .pipe(rename({basename: '_' + options.fontName}))
            // set path to export your CSS
            .pipe(gulp.dest('src/sass'));
      })
      .pipe(gulp.dest(workDir + 'fonts'));
});


gulp.task('sass', ['clean-css'], function() {
  return gulp.src(conf.css.src, {base: conf.css.min})
      .pipe(plumber())
      .pipe(compileSass())
      .pipe(gulp.dest(conf.css.min));
});


gulp.task('sass-dev', function() {
  return gulp.src(conf.css.src, {base: conf.css.min})
      .pipe(cached('sass-dev-cache', {optimizeMemory: true}))
      .pipe(plumber())
      .pipe(progeny({extensionsList: ['scss']}))
      .pipe(compileSass())
      .pipe(gulp.dest(conf.css.min))
      .pipe(count('Updated ## sass files (with maps)'))
      .pipe(browserSync.stream({match: '**/*.css'}));
});


gulp.task('vue', function() {
  process.env.NODE_ENV = 'production';

  const webpackConfig = './webpack.config.js';

  if (fs.existsSync(webpackConfig)) {
    return gulp.src(webpackConfig)
        .pipe(webpack(require(webpackConfig)))
        .pipe(gulp.dest(conf.js.min));
  }
});


gulp.task('browser-sync', function() {
  browserSync.init({
    proxy: 'http://ihumanity/html/index.html',
    watchOptions: {
      ignoreInitial: true
    },
    // reloadDelay: 100
  });
});


gulp.task('watcher', (callback) => {
  runSequence(
      ['browser-sync'], // потом запускаем браузерсинк
      ['sass-dev', 'vue'], // сначала билдим стили и скрипты
      callback
  );


  // Для избежания утечек памяти на Windows в idea нужно выключать опцию
  // "use safe write" Settings => Appearance & Behaviour => System settings
  watch(conf.css.src, {read: false}, function() {
    runSequence(
        ['sass-dev']
    )
  });

  watch(conf.js.min + '/**/*.js', {read: false}, function () {
    browserSync.reload();
  });

  watch(conf.html, {read: false}, function () {
    browserSync.reload();
  });
});


gulp.task('default', (callback) => {
  runSequence(
      // 'sprite',
      'icon-font',
      ['sass'],
      callback
  );
});
