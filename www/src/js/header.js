import Vue from '../../node_modules/vue/dist/vue';
import {store} from './store.js';
import ContextPopup from './context-popup.vue';


new Vue({
  el: '.main-header',

  store,

  components: {
    ContextPopup
  },

  data: {
    mobileSearchVisible: false
  },

  methods: {
    showMobileSearch() {
      this.mobileSearchVisible = true;
    }
  }
});
